// ignore_for_file: prefer_const_constructors
import 'package:flutter/material.dart';

import 'package:nfc_manager/nfc_manager.dart';
import 'package:nfc_component/view/common/form_row.dart';
import 'package:nfc_component/view/common/nfc_session.dart';
import 'package:nfc_component/view/ndef_record.dart';
import 'package:provider/provider.dart';

class TagReadModel with ChangeNotifier {
  NfcTag? tag;

  Map<String, dynamic>? additionalData;

  Future<String?> handleTag(NfcTag tag) async {
    this.tag = tag;
    additionalData = {};

    notifyListeners();
    return 'Scannen is gelukt!';
  }
}

class TagReadPage extends StatelessWidget {
  static Widget withDependency() => ChangeNotifierProvider<TagReadModel>(
        create: (context) => TagReadModel(),
        child: TagReadPage(),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('Activiteit Tracker'),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.all(2),
        children: [
          FormSection(
            children: [
              FormRow(
                title: Center(
                  child: Text('Scan een tag',
                      style: TextStyle(color: Colors.white)),
                ),
                onTap: () => startSession(
                  context: context,
                  handleTag: Provider.of<TagReadModel>(context, listen: false)
                      .handleTag,
                ),
              ),
            ],
          ),
          Consumer<TagReadModel>(builder: (context, model, _) {
            final tag = model.tag;
            final additionalData = model.additionalData;
            if (tag != null && additionalData != null) {
              return _TagInfo(tag, additionalData);
            }
            return SizedBox.shrink();
          }),
        ],
      ),
    );
  }
}

class _TagInfo extends StatelessWidget {
  _TagInfo(this.tag, this.additionalData);

  final NfcTag tag;

  final Map<String, dynamic> additionalData;

  @override
  Widget build(BuildContext context) {
    final ndefWidgets = <Widget>[];

    Object? tech;
    tech = Ndef.from(tag);
    if (tech is Ndef) {
      final cachedMessage = tech.cachedMessage;
      if (cachedMessage != null) {
        Iterable.generate(cachedMessage.records.length).forEach(
          (i) {
            final record = cachedMessage.records[i];
            final info = NdefRecordInfo.fromNdef(record);
            ndefWidgets.add(
              FormRow(
                title: Text('${info.title}'),
                subtitle: Text('${info.subtitle}'),
                trailing: null,
              ),
            );
          },
        );
      }
    }

    return Column(
      children: [
        if (ndefWidgets.isNotEmpty)
          FormSection(
            header: Text('Activiteit'),
            children: ndefWidgets,
          ),
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: TextFormField(
            decoration: InputDecoration(labelText: 'Specificatie'),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: TextFormField(
            decoration: InputDecoration(labelText: 'Opmerkingen'),
          ),
        ),
        SizedBox(
          height: 150,
        ),
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.deepPurpleAccent),
              ),
              child: Text('Verstuur'),
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TagReadPage.withDependency(),
                        )),
                  }),
        )
      ],
    );
  }
}
