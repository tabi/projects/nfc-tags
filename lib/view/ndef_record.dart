// ignore_for_file: prefer_const_constructors
import 'package:nfc_component/model/record.dart';
import 'package:nfc_manager/nfc_manager.dart';

class NdefRecordInfo {
  const NdefRecordInfo(
      {required this.record, required this.title, required this.subtitle});

  final Record record;

  final String title;

  final String subtitle;

  static NdefRecordInfo fromNdef(NdefRecord record) {
    final _record = Record.fromNdef(record);
    if (_record is WellknownTextRecord) {
      return NdefRecordInfo(
        record: _record,
        title: _record.text,
        subtitle: 'NFC Tag',
      );
    }
    throw UnimplementedError();
  }
}
